import React, { Component, PropTypes } from 'react';
import {Modal} from 'react-bootstrap/lib';

export default class AlertMessage extends Component {
  static propTypes = {
    show: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    message: PropTypes.object,
    title: PropTypes.string,
    actions: PropTypes.object,
    data: PropTypes.object
  };

  getModal() {
    const {actions, message, data} = this.props;
    const closeModal = actions.cancel;
    const confirm = data ? actions.confirm.bind(null, data) : actions.confirm;

    return (
      <Modal
        bsSize="large"
        dialogClassName="alert-message-modal"
        className="container--pageroute modal--addjob"
        show={this.props.show}
        onHide={closeModal}
      >
        <Modal.Body>
          <div className="row">
            <div className="col-xs-12 generic-page__rightColumn div--surveyMatchModalBody">

              <span className="icon-cancel pointer pull-right span__editSurveyModal--close" onClick={closeModal} />
              <h4 className="heading modal--heading text-center">{this.props.title}</h4>
              <div className="div__modal--content">
                {message}
              </div>

              <div className="button__container text-right">
                <button className="btn btn-primary cancel" onClick={closeModal}>
                  Cancel
                </button>
                <button className="btn btn-primary save" onClick={confirm}>
                  Continue
                </button>
              </div>

            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    if (!this.props.show) {
      return (!React.Children.count(this.props.children)) ? null : this.props.children;
    }

    if (!React.Children.count(this.props.children)) {
      return this.getModal();
    }

    return (
      <div style={{position: 'relative'}}>
        {this.getModal()}
        {this.props.children}
      </div>
    );
  }
}
