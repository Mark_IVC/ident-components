import React, { Component, PropTypes } from 'react';
import {Modal, Form, FormGroup, FormControl, ControlLabel} from 'react-bootstrap/lib';
import ReactSelect from 'react-select';

export default class CompSetEditModal extends Component {
  static propTypes = {
    userGroups: PropTypes.object
  };

  componentDidMount() {
    this.props.userGroups.getOrgType();
  }

  change(info, event) {
    // console.log('change event: ', 'info: ', info, 'eventvalues: ', event.target.value, event.target.name, event.target.type);

    if (info.name === 'government_contractor') info.value = (event.target.value == 'true');
    else info.value = event.target.value;

    if (info.name === 'employees_count' || info.name === 'total_revenue') {
      info.value = event.target.value !== '' ? parseInt(event.target.value, 10) : null;
    }

    if (info.name === 'user_group_name') {
      info.value = event.target.value.slice(0, 34);
    }
    this.props.userGroups.changeItemValue(info);
  }

  loadIndustryOptions(input, callback) {
    if (input && input.length >= 2) {
      // console.log('input for query: ', input);
      this.props.userGroups.getIndustryOptions({
        params: {
          numResults: null,
          industrySearchTerm: input
        },
        searchCallback: callback
      });
    } else {
      callback(null, {options: [], complete: false});
    }
  }

  handleIndustryChange(selection) {
    if (selection) {
      this.change({type: 'string', name: 'industry'}, {target: {value: selection.label}});
    } else {
      this.change({type: 'string', name: 'industry'}, {target: {value: ''}});
    }
  }

  handleCompanyTypeChange(info, selection) {
    // when user hits x, selection is null
    if (!selection) return this.change(info, {target: {value: null}});

    this.change(info, {target: {value: selection.label}});
  }

  render() {
    const {currentEditItem, validation, saveItem, orgTypes} = this.props.userGroups;

    return (
      <Modal
        bsSize="large"
        className="container--pageroute"
        // !! -> to satisfy the props validation
        show={!!this.props.userGroups.currentEditItem}
        onHide={this.props.userGroups.closeEditItem}
      >
        <Modal.Body className="compsets-page__modal">

          <h4 className="compsets-page__title">Add/Edit User Group </h4>

          <div className="row">

            <div className="col-md-5">
              <Form >
                <FormGroup validationState={validation.user_group_name}>
                  <label className="label__form--addJob label__form--basicJobInfo">
                    User Group Name
                  </label>
                  <FormControl
                    type="string"
                    value={currentEditItem.user_group_name}
                    onChange={this.change.bind(this, {type: 'string', name: 'user_group_name'})}
                  />
                </FormGroup>

                <FormGroup validationState={validation.company_type}>
                  <label className="label__form--addJob label__form--basicJobInfo">
                    (select example) Organization type
                  </label>
                  <ReactSelect
                    name="form-field-name"
                    value={currentEditItem.company_type}
                    options={orgTypes}
                    onChange={this.handleCompanyTypeChange.bind(this, {type: 'string', name: 'company_type'})}
                  />
                </FormGroup>
              </Form>
            </div>

            <div className="col-md-2">

            </div>

            <div className="col-md-5">
              <Form >

                <FormGroup controlId="jobFormInfo" validationState={validation.industry}>
                  <label className="label__form--addJob label__form--basicJobInfo">
                    (async select example) Industry
                  </label>
                  <ReactSelect.Async className={'input__form--addJob ' + validation.industry}
                                value={currentEditItem.industry}
                                onChange={this.handleIndustryChange.bind(this)}
                                placeholder={currentEditItem.industry}
                                loadOptions={this.loadIndustryOptions.bind(this)}
                  />
                </FormGroup>

                <FormGroup validationState={validation.government_contractor}>
                  <div className="gov_contractor_checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={currentEditItem.government_contractor}
                        onChange={this.change.bind(this, {type: 'string', name: 'government_contractor'})}
                        value={!currentEditItem.government_contractor}
                      />
                      (example) Government Contractor
                    </label>
                  </div>
                </FormGroup>

              </Form>
            </div>

          </div>

          <div className="button__container text-right">
            <button className="btn btn-primary cancel" onClick={this.props.userGroups.closeEditItem}>
              Cancel
            </button>
            <button className="btn btn-primary save" onClick={saveItem.bind(null, currentEditItem, validation)}>
              Save
            </button>
          </div>

          <div className="clear" />

        </Modal.Body>
      </Modal>
    );
  }
}
