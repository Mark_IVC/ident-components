import React, { Component, PropTypes } from 'react';
// component files
import UserGroupEditModal from './UserGroupEditModal';
import AlertMessage from './AlertMessage';
import Helmet from 'react-helmet';

export default class UserGroups extends Component {
  static propTypes = {
    userGroups: PropTypes.object,
    getUserGroupsActions: PropTypes.func
  };

  componentDidMount() {
    this.props.getUserGroupsActions()
      .getItems();
  }

  getRow(row, userGroups) {
    return (
      <div className="row compsets--tile__row" key={row.user_group_pk}>
        <div className="col-md-3">
          Name: {row.user_group_name}
        </div>
        <div className="col-md-3">
          owner user id: {row.creator_user_id}
        </div>
        <div className="col-md-3">
          last modified: {row.last_modified_time}
        </div>
        <div className="col-md-1 pointer" onClick={userGroups.editItem.bind(null, row)}>
          EDIT
        </div>
        <div className="col-md-1 pointer" onClick={userGroups.deleteAlert.bind(null, row)}>
          DELETE
        </div>
      </div>
    );
  }

  render() {
    const userGroups = {...this.props.userGroups, ...this.props.getUserGroupsActions()};

    const rows = userGroups.items;

    const messageForAlert = <p>This is the confirmation message to delete an item.</p>;

    return (
      <div className="container-fluid page__container compsets-page__container">
        <Helmet title={'User Groups'}/>

        <div className="row">
          <div className="col-md-12">
            <h4 className="compsets-page__title">
              User Groups
              <span className="icon-plus-with-circle pointer editLink--green compsets__addIcon showIcon"
                    onClick={userGroups.addItem}>
              </span>
            </h4>
          </div>
        </div>
        {
          rows.map(row => this.getRow(row, userGroups))
        }
        <AlertMessage
          show={userGroups.showDeleteAlert}
          data={userGroups.itemDataToDelete}
          message={messageForAlert}
          title="Warning"
          actions={{
            confirm: userGroups.deleteItem,
            cancel: userGroups.cancelDeleteAlert
          }}
        />
        <UserGroupEditModal userGroups={userGroups} />
      </div>
    );
  }
}
