import React, { Component, PropTypes } from 'react';
import {Button, Col, ControlLabel, Form, FormGroup, FormControl, HelpBlock, InputGroup} from 'react-bootstrap';

class AddUser extends Component {
  static propTypes = {

  };

  constructor(props) {
    super(props);

    this.state = {
      helpMessage: ''
    }
  }

  render() {
    console.log('Log in IdentityManagement.AddUser');

     return (
      <div>
        <div className="row div__row--editProfileContainer">
          <Form className="form--editProfile">
            <div className="div--profileFormWidth">
              <div className="div__form--inputContainer col-sm-7">
                <FormGroup>
                  <h3 className="heading--profileHeading">Add User</h3>
                </FormGroup>
                <FormGroup>
                  <ControlLabel className="label--profileForm">First Name</ControlLabel>
                  <FormControl
                    id="fn"
                    type="text"
                    value={this.state.first}
                    className="profile__input"
                  />
                </FormGroup>
                <FormGroup>
                  <ControlLabel className="label--profileForm">Last Name</ControlLabel>
                  <FormControl
                    id="ln"
                    type="text"
                    value={this.state.last}
                    className="profile__input"
                  />
                </FormGroup>
                <Button type="button" className="profile__button">
                  Add User
                </Button>
              </div>
            </div>
          </Form>

          <div className="div__container--passwordNotification validator__help--sizing">
            <FormGroup>
              <HelpBlock>
                <span className="password__helpblock--text">{ this.state.helpMessage }</span>
              </HelpBlock>
            </FormGroup>
          </div>
        </div>
      </div>
    );
  }
}

export default AddUser;
