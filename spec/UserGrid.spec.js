import React from 'react';
import UserGrid from '../src/components/UserGrid.js';
import { shallow } from 'enzyme';
import { expect } from 'chai';

describe('UserGrid', function() {

  const output = shallow(
    <UserGrid />
  );

  it('should render', function() {
    expect(output.find('.UserGrid').length.to.equal(1));
  });
});
